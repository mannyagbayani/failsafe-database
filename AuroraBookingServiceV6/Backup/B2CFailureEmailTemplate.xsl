<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:msxsl="urn:schemas-microsoft-com:xslt"
    xmlns:util="util:my-scripts">
	<msxsl:script language="C#" implements-prefix="util">
		<![CDATA[
	 public string getCurrentDate()
	 {
		return System.DateTime.Now.ToString("dd-MMM-yyyy");
	 }
	 
	 public string translateStatusCode(string statusCode)
	 {
		switch (statusCode)
		{
			case "KK" :
						return "Confirmed";
						break;
			case "KB" :
						return "Knocked-Back";
						break;
			case "QN" :
						return "Quoted";
						break;
			case "WL" :
						return "Waitlisted";
						break;
			
			default : 
			return "Unknown Status";
		}
	 }
	 
	 public string getEmailSubjectStringLink(string vehicleCode, string surname, string bookingNumber)
	 {
	     return @"mailto:helpdesk@thlonline.com?subject=" + 
				vehicleCode + " - " + surname + " " + bookingNumber +" : Error in B2C Booking Transfer";
	 }
      ]]>
	</msxsl:script>
	<xsl:template match="/">
		<HTML>
			<HEAD>
				<style>
					BODY
					{
						font-family: Courier New;
					}

					TD
					{
						font-size: 8pt;
					}

					A
					{
						color: Red;
					}

					.CellLabel
					{
						font-weight: bolder;
					}

					.Warning
					{
						font-size: 11pt;
						color: Red;
					}

					.WarningSmall
					{
						color: Red;
						font-size: 8pt;
					}

					.DebugInfo
					{
						background-color:#efefef
					}
				</style>
			</HEAD>
			<body>
				<h1 align="center">
					Error in B2C booking transfer to Aurora
				</h1>
				<h2 align="center">
					Please enter this booking into Aurora manually
				</h2>
				<br/>
				<table border="0" width="90%" align="center">
					<thead>
						<tr>
							<th colspan="4" align="left">
								Booking Information
							</th>
						</tr>
					</thead>
					<tr>
						<td align="left" nowrap="" class="CellLabel" width="10%">
							Booking Status
						</td>
						<td width="30%">
							<xsl:value-of  select="util:translateStatusCode(Data/RentalDetails/Status)"/>
						</td>
						<td class="CellLabel" width="10%">
							Date
						</td>
						<td width="50%">
							<xsl:value-of  select="util:getCurrentDate()"/>
						</td>
					</tr>
					<tr>
						<td class="CellLabel">
							Booking Number
						</td>
						<td>
							<xsl:value-of select="Data/RentalDetails/AddBookingNumber"/>
						</td>
						<td class="CellLabel">
							Payment Status
						</td>

						<xsl:if test="Data/CreditCardDetails/CreditCardNumber !=''">
							<td class="WarningSmall">
								<a>
									** Please don&#39;t process payment ** Customer has paid $<xsl:value-of select="Data/CreditCardDetails/PaymentAmount"/>
								</a>
							</td>
						</xsl:if>
						<xsl:if test="Data/CreditCardDetails/CreditCardNumber =''">
							<td>
								No payment collected
							</td>
						</xsl:if>

					</tr>
					<tr>
						<td class="CellLabel">
							Vehicle
						</td>
						<td>
							<xsl:value-of select="Data/Info/VehicleCode"/>
						</td>
						<td class="CellLabel">
							Package
						</td>
						<td>
							<xsl:value-of select="Data/Info/PackageCode"/>
						</td>
					</tr>
					<tr>
						<td colspan="4">
							<hr color="black" width="100%"/>
						</td>
					</tr>
					<thead>
						<tr>
							<th colspan="4" align="left">
								Customer Information
							</th>
						</tr>
					</thead>
					<tr>
						<td class="CellLabel">
							Customer
						</td>
						<td>
							<xsl:value-of select="Data/CustomerData/Title"/>&#160;<xsl:value-of select="Data/CustomerData/FirstName"/>&#160;<xsl:value-of select="Data/CustomerData/LastName"/>
						</td>
						<td class="CellLabel">
							Email
						</td>
						<td>
							<xsl:value-of select="Data/CustomerData/Email"/>
						</td>
					</tr>
					<tr>
						<td class="CellLabel">
							Country
						</td>
						<td>
							<xsl:value-of select="Data/CustomerData/CountryOfResidence"/>
						</td>
						<td class="CellLabel">
							Phone
						</td>
						<td>
							<xsl:value-of select="Data/CustomerData/PhoneNumber"/>
						</td>
					</tr>
					<tr>
						<td class="CellLabel">
							Agent Code
						</td>
						<td>
							<xsl:value-of select="Data/RentalDetails/AgentReference"/>
						</td>
						<td>
							&#160;
						</td>
						<td>
							&#160;
						</td>
					</tr>
					<tr>
						<td class="CellLabel">
							Customer Comments
						</td>
						<td colspan="3">
							<xsl:value-of select="Data/RentalDetails/Note"/>
						</td>
					</tr>
					<tr>
						<td colspan="4">
							<hr color="black" width="100%"/>
						</td>
					</tr>
					<thead>
						<tr>
							<th colspan="4" align="left">
								Pickup/Dropoff Information
							</th>
						</tr>
					</thead>
					<tr>
						<td class="CellLabel">
							Check Out
						</td>
						<td nowrap="">
							<xsl:value-of select="Data/RentalDetails/CheckOutDateTime"/>
						</td>
						<td class="CellLabel">
							Branch Code
						</td>
						<td nowrap="">
							<xsl:value-of select="Data/RentalDetails/CheckOutLocationCode"/>
						</td>
					</tr>
					<tr>
						<td class="CellLabel">
							Check In
						</td>
						<td>
							<xsl:value-of select="Data/RentalDetails/CheckInDateTime"/>
						</td>
						<td class="CellLabel">
							Branch Code
						</td>
						<td nowrap="">
							<xsl:value-of select="Data/RentalDetails/CheckInLocationCode"/>
						</td>
					</tr>
					<tr>
						<td colspan="4">
							<hr color="black" width="100%"/>
						</td>
					</tr>
				</table>
				<table border="0" width="90%" align="center">
					<thead>
						<tr>
							<th colspan="3" align="left">
								Extra Hire Item Information
							</th>
						</tr>
					</thead>
					<tr>
						<td class="CellLabel" width="40%">
							Product Name
						</td>
						<td class="CellLabel" align="right">
							Quantity
						</td>
						<td width="50%">

						</td>
					</tr>
					<xsl:for-each select="/Data/RentalDetails/ProductList/Product">
						<tr>
							<td width="40%">
								<xsl:value-of select="@PrdShortName"/>
							</td>
							<td align="right">
								<xsl:value-of select="@Qty"/>
							</td>
						</tr>
					</xsl:for-each>
					<tr>
						<td colspan="3">
							<hr color="black" width="100%" />
						</td>
					</tr>
					<thead>
						<tr>
							<th colspan="3" align="left">
								Error Information
							</th>
						</tr>
					</thead>
					<tr>
						<td colspan="3" class="DebugInfo">
							<xsl:value-of select="Data/DebugInfo"/>
						</td>
					</tr>
					<tr>
						<td colspan="3">
							<hr color="black" width="100%" />
						</td>
					</tr>
					<thead>
						<tr>
							<th colspan="3" align="center" class="Warning">
								<br />
								<p>
									This is an automatically generated message, please do not reply
									<br />
									For all queries about this failed booking please contact the
									<xsl:element name="a">
										<xsl:attribute name="href">
											<xsl:value-of select="util:getEmailSubjectStringLink(Data/Info/VehicleCode,Data/CustomerData/LastName,Data/RentalDetails/AddBookingNumber)"/>
										</xsl:attribute>
										helpdesk
									</xsl:element>
								</p>
							</th>
						</tr>
					</thead>
				</table>
			</body>
		</HTML>
	</xsl:template>
</xsl:stylesheet>
