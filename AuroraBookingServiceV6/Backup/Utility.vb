Imports System.Configuration
Imports System.Data
Imports System.Data.SqlClient
Imports System.Xml
Imports System.Net.Mail
Imports System.ServiceProcess
Imports System.Xml.Xsl
Imports System.IO

Public Class Utility

#Region "Variables"
    Dim WS As New PHOENIX.PhoenixWebService
#End Region

#Region "Constant"
    Const EMAILSENT As String = "EmailSent"
    Const CREATED As String = "Created"
    Const BOOKINGELEMENT As String = "<AddBookingNumber></AddBookingNumber>"
    Const EVENTSOURCENAME As String = "AuroraBookingServiceV5"
#End Region

#Region "Readonly properties"

    Private _IsFirstLoad As Boolean
    Public Property IsFirstLoad() As Boolean
        Get
            Return _IsFirstLoad
        End Get
        Set(ByVal value As Boolean)
            _IsFirstLoad = value
        End Set
    End Property


    Private _ConnectionString As String
    Public ReadOnly Property ConnectionString() As String
        Get
            Return _ConnectionString
        End Get
    End Property


    Private _NoOfAttempts As Integer
    Public ReadOnly Property NoOfAttempts() As Integer
        Get
            Return _NoOfAttempts
        End Get
    End Property


    Private _maintainLogged As Boolean
    Public ReadOnly Property maintainLogged() As Boolean
        Get
            Return _maintainLogged
        End Get
    End Property

    Private _minutesToRefresh As Integer
    Public ReadOnly Property minutesToRefresh() As Integer
        Get
            Return _minutesToRefresh
        End Get
    End Property

    Private _ResTeamEmail As String
    Private ReadOnly Property ResTeamEmail() As String
        Get
            Return _ResTeamEmail
        End Get
    End Property

    Private _smtp As String
    Private ReadOnly Property SMTP() As String
        Get
            Return _smtp
        End Get
    End Property


    Private _from As String
    Private ReadOnly Property From() As String
        Get
            Return _from
        End Get
    End Property

    Private _subject As String
    Private ReadOnly Property Subject() As String
        Get
            Return _subject
        End Get
    End Property

    Private _body As String
    Private ReadOnly Property Body() As String
        Get
            Return _body
        End Get
    End Property

    Private _threadsleep As String
    Private ReadOnly Property ThreadSleep() As String
        Get
            Return _threadsleep
        End Get
    End Property


    Public ReadOnly Property WSUrl() As String
        Get
            Return WS.Url.ToString
        End Get
    End Property


    Private _XmlString As String
    Private ReadOnly Property XmlString() As String
        Get
            Return _XmlString
        End Get
    End Property

    Private _boonum As String
    Private ReadOnly Property BooNum() As String
        Get
            Return _boonum
        End Get
    End Property
#End Region

#Region "Procedures"
    Private Sub CreateEventLogging()
        If maintainLogged Then
            If Not EventLog.SourceExists(EVENTSOURCENAME) Then
                EventLog.CreateEventSource(EVENTSOURCENAME, "LOGS")
            End If
            ''LoggedEntry("WS: " & WS.Url.ToString & ", DB: " & _ConnectionString)
        End If
    End Sub
    Public Sub LoggedEntry(ByVal strInfo As String, _
                            Optional ByVal eventlogentry As EventLogEntryType = EventLogEntryType.Information)
        If Not maintainLogged Then Exit Sub
        Dim mylog As New EventLog
        mylog.Source = EVENTSOURCENAME
        mylog.WriteEntry(strInfo, eventlogentry)
    End Sub
    Sub WSWrapper(ByVal BooNum As String, ByVal requestXMLNode As Xml.XmlNode, Optional ByVal xmlRequest As String = "")
        Dim Attempts As Integer = 1

        Dim result As Xml.XmlDocument = New Xml.XmlDocument
        Dim tempString As String = ""
        Dim isValidXML As Boolean = True

        Dim xmlWSCallResult As XmlNode

        If UpdateBookingSucced(BooNum, requestXMLNode, 1, xmlRequest, xmlWSCallResult) Then
            Exit Sub
        Else
            Try
                Dim succeed As Boolean = False

                Do While Attempts <= Me.NoOfAttempts
                    succeed = UpdateBookingSucced(BooNum, requestXMLNode, Attempts, "", xmlWSCallResult)
                    Threading.Thread.Sleep(Me.ThreadSleep)
                    Attempts = Attempts + 1
                Loop

                If (Not succeed) Then
                    ModifiedStatus(BooNum, EMAILSENT, Me.NoOfAttempts, xmlRequest)

                    _boonum = BooNum
                    _XmlString = requestXMLNode.OuterXml
                    SendEmail(requestXMLNode.OuterXml, BooNum, xmlWSCallResult)
                End If

            Catch ex As Exception
                LoggedEntry("WSWrapper: " & ex.Message & "," & ex.Source, EventLogEntryType.Error)
            End Try
        End If

        result = Nothing
    End Sub
#End Region

#Region "private Function"

    Private Function Con() As SqlConnection
        Return New SqlConnection(Me.ConnectionString)
    End Function

    Private Function ReadConfigFile() As Boolean
        ReadConfigFile = True
        Try
            _ConnectionString = ConfigurationManager.ConnectionStrings("connectionStrings").ConnectionString
            _NoOfAttempts = Convert.ToInt16(ConfigurationManager.AppSettings("NoOfAttempts"))
            _maintainLogged = ConfigurationManager.AppSettings("MaintainLogged")
            _minutesToRefresh = ConfigurationManager.AppSettings("minutesToRefresh")
            _ResTeamEmail = ConfigurationManager.AppSettings("ResTeamEmail")
            _smtp = ConfigurationManager.AppSettings("SMTP")
            _from = ConfigurationManager.AppSettings("From")
            _subject = ConfigurationManager.AppSettings("Subject")
            _body = ConfigurationManager.AppSettings("Body")
            _threadsleep = ConfigurationManager.AppSettings("ThreadSleep")
        Catch ex As Exception
            ReadConfigFile = False
            LoggedEntry("Failed on ReadConfigFile", EventLogEntryType.Error)
        End Try
    End Function

#End Region

#Region "public function"

    Public Function LoadBookingDS() As DataSet
        Dim ds As New DataSet
        Dim da As SqlDataAdapter
        Try
            da = New SqlDataAdapter("WinSer_GetBookingData", Con)
            da.Fill(ds)
        Catch ex As Exception
            ''logging exception
            LoggedEntry("Failed on LOadBookingDS", EventLogEntryType.Error)
            ds = Nothing
        End Try
        Return ds
    End Function

    Private Function IsTransactionStatusIsNull(ByVal Boonum As String) As Boolean
        Dim cmd As SqlCommand = Nothing
        Dim tempCon As SqlConnection
        Try
            tempCon = Con()
            If tempCon.State = ConnectionState.Closed Then tempCon.Open()
            cmd = New SqlCommand("WinSer_GetBookingDataFiltered", tempCon)
            cmd.Parameters.Add(New SqlParameter("BooNum", SqlDbType.NVarChar, 15)).Value = Boonum
            cmd.CommandType = CommandType.StoredProcedure
            Dim retvalue As String = cmd.ExecuteScalar

            If (String.IsNullOrEmpty(retvalue)) Then
                ''send email
                Return True
            Else
                ''dont send email
                Return False
            End If

        Catch ex As Exception
            LoggedEntry(Boonum & ":" & ex.Message & "," & ex.StackTrace, EventLogEntryType.Error)
        End Try
        tempCon = Nothing
        cmd = Nothing

    End Function

    Public Function ModifiedStatus(ByVal boonum As String, _
                                   ByVal transactionStatus As String, _
                                   ByVal noOfAttempts As Integer, _
                                   Optional ByVal newRequextxml As String = "") As Integer

        Dim retNo As Integer = -1
        Dim xTrans As SqlTransaction = Nothing
        Dim cmd As SqlCommand = Nothing
        Dim tempCon As SqlConnection
        Try
            tempCon = Con()
            If tempCon.State = ConnectionState.Closed Then tempCon.Open()
            xTrans = tempCon.BeginTransaction("ModifedStatus")
            cmd = New SqlCommand("WinSer_UpdateBookingStatus", tempCon)
            cmd.Transaction = xTrans
            cmd.Parameters.Add(New SqlParameter("BooNum", SqlDbType.NVarChar, 15)).Value = boonum
            cmd.Parameters.Add(New SqlParameter("TransactionStatus", SqlDbType.NVarChar, 10)).Value = transactionStatus
            cmd.Parameters.Add(New SqlParameter("NoOfAttempt", SqlDbType.Int)).Value = noOfAttempts
            cmd.Parameters.Add(New SqlParameter("requestXML", SqlDbType.Text)).Value = newRequextxml

            cmd.CommandType = CommandType.StoredProcedure
            retNo = cmd.ExecuteNonQuery
            xTrans.Commit()
        Catch ex As Exception
            LoggedEntry(boonum & ":" & ex.Message & "," & ex.StackTrace, EventLogEntryType.Error)
            xTrans.Rollback()
        End Try
        xTrans = Nothing
        tempCon = Nothing
        cmd = Nothing

        Return retNo
    End Function

    Public Function PushToAurora() As String
        Dim ds As DataSet = Nothing
        Dim retValue As String = "OK"
        Dim xmlstring As String = ""
        Dim AddBookingNumber As String = ""
        Dim booNum As String = ""

        Dim xmlDom As New Xml.XmlDocument
        Dim isOK As Boolean = True
        Try
            Try
                ds = LoadBookingDS()
                If Not ds Is Nothing Then
                    If ds.Tables(0).Rows.Count = 0 Then
                        LoggedEntry("Dataset table is empty", EventLogEntryType.Warning)
                        Return "No Data to process"
                    End If
                End If
            Catch ex As Exception
                LoggedEntry("ERROR: ds = LoadBookingDS: " & ex.Message, EventLogEntryType.Warning)
            End Try


            If Not ds Is Nothing Then
                Dim row As DataRow = ds.Tables(0).Rows(0) ''Nothing

                If TypeOf row("BooNum") Is DBNull And TypeOf row("RequestXML") Is DBNull Then
                    isOK = False
                ElseIf (Not TypeOf row("BooNum") Is DBNull And TypeOf row("RequestXML") Is DBNull) Then
                    isOK = False
                ElseIf (TypeOf row("BooNum") Is DBNull And Not TypeOf row("RequestXML") Is DBNull) Then
                    isOK = False
                Else
                    isOK = True
                End If
                If isOK Then
                    xmlstring = row("RequestXML").ToString.TrimEnd
                    booNum = row("BooNum").ToString.TrimEnd

                    If xmlstring.Contains("</data>") Then
                        xmlstring = xmlstring.Replace("</data>", "</Data>")
                        xmlstring = xmlstring.Replace("<data xmlns="""">", "<Data xmlns="""">")
                    End If

                    xmlDom.LoadXml(xmlstring)


                    IsFirstLoad = True

                    If Not xmlDom.SelectSingleNode("Data/RentalDetails/AddBookingNumber") Is Nothing Then
                        xmlDom.SelectSingleNode("Data/RentalDetails/AddBookingNumber").InnerText = "B" & booNum
                    End If
                    WSWrapper(booNum, xmlDom.DocumentElement, xmlDom.OuterXml)

                End If
            End If


        Catch xmlEr As Xml.XmlException
            LoggedEntry("XML: " & xmlEr.Message & "," & xmlEr.LinePosition, EventLogEntryType.Error)
            retValue = "Failed"
        Catch ex As Exception
            LoggedEntry("APP: " & ex.Message & "," & ex.Source, EventLogEntryType.Error)
            retValue = "Failed"
        End Try
        ds = Nothing
        Return retValue
    End Function

    Public Function UpdateBookingSucced(ByVal BooNum As String, _
                                 ByVal requestXMLNode As Xml.XmlNode, _
                                 Optional ByVal attempts As Integer = 1, _
                                 Optional ByVal xmlRequest As String = "", _
                                 Optional ByRef xmlResult As Xml.XmlNode = Nothing) As Boolean

        Dim result As Xml.XmlDocument = New Xml.XmlDocument
        Dim tempString As String = ""
        Dim isValidXML As Boolean = True
        Dim sbXmlResultString As New Text.StringBuilder

        Try
            ' test for presence of avpid
            sbXmlResultString.Append("<WSData>")

            If GetInnerText(requestXMLNode.SelectSingleNode("RentalDetails/AvpId")) = "" Then
                ' if no avpid, then call force create
                tempString = WS.ForceCreateBooking(requestXMLNode).OuterXml
                'xmlResult = WS.ForceCreateBooking(requestXMLNode)
                sbXmlResultString.Append("<WebMethodName>ForceCreateBooking</WebMethodName>")
            Else
                ' else call create booking
                tempString = WS.CreateBookingWithExtraHireItemAndPayment(requestXMLNode).OuterXml
                'xmlResult = WS.CreateBookingWithExtraHireItemAndPayment(requestXMLNode)
                sbXmlResultString.Append("<WebMethodName>CreateBookingWithExtraHireItemAndPayment</WebMethodName>")
            End If

            sbXmlResultString.Append("<WebServiceReturned>" & tempString & "</WebServiceReturned>")
            sbXmlResultString.Append("</WSData>")
            Dim xmlTemp As New XmlDocument
            xmlTemp.LoadXml(sbXmlResultString.ToString())
            xmlResult = CType(xmlTemp, XmlNode)

            Threading.Thread.Sleep(Me.ThreadSleep)
            result.LoadXml(tempString)
            isValidXML = True
        Catch ex As Exception
            LoggedEntry("LoadXml:" & ex.Message & "," & ex.Source, EventLogEntryType.Error)
            isValidXML = False
        End Try

        If isValidXML Then
            If result.SelectSingleNode("Error/Message") IsNot Nothing Then
                If Not String.IsNullOrEmpty(result.SelectSingleNode("Error/Message").InnerText) Then
                    UpdateBookingSucced = False
                End If
            End If

            If result.SelectSingleNode("Data/Booking/Message") IsNot Nothing Then
                If Not String.IsNullOrEmpty(result.SelectSingleNode("Data/Booking/Message").InnerText) Then
                    If result.SelectSingleNode("Data/Booking/Message").InnerText <> "Success" Then
                        UpdateBookingSucced = False
                    Else
                        ModifiedStatus(BooNum, CREATED, attempts, xmlRequest)
                        UpdateBookingSucced = True
                        result = Nothing
                    End If
                End If
            End If
        End If
        Return UpdateBookingSucced
    End Function

    Public Function SendEmail(ByVal xml As String, Optional ByVal BooNum As String = "", Optional ByVal xmlWSCallResult As Xml.XmlNode = Nothing) As Boolean
        SendEmail = True
        Try
            Dim xmlInputData As New XmlDocument
            xmlInputData.LoadXml(xml)

            If IsFirstLoad = False Then Return False
            If String.IsNullOrEmpty(SMTP) Then Exit Function
            Dim tempBody As String = BodyFormat(xml, xmlWSCallResult)
            Dim sSubject As String = ""

            ' [$VehicleCode] - [$CustomerSurname] [$BookingNumber] : Error in B2C Booking Transfer
            With xmlInputData.DocumentElement
                sSubject = Subject.Replace("[$VehicleCode]", GetInnerText(.SelectSingleNode("Info/VehicleCode"))).Replace("[$CustomerSurname]", GetInnerText(.SelectSingleNode("CustomerData/LastName"))).Replace("[$BookingNumber]", GetInnerText(.SelectSingleNode("RentalDetails/AddBookingNumber")))
            End With

            Dim message As New MailMessage(From, ResTeamEmail, sSubject, tempBody)
            'String.Concat("B" & BooNum, Subject), tempBody)
            message.IsBodyHtml = True
            Dim emailClient As New SmtpClient(SMTP)
            emailClient.Port = CInt(ConfigurationManager.AppSettings("SMTPport"))
            emailClient.Send(message)

        Catch ex As Exception
            LoggedEntry("Send Email: " & ex.Message & vbNewLine & ex.StackTrace)
            Return False
        End Try

        Return True
    End Function

    Public Function SendEmail() As Boolean
        Return SendEmail(Me.XmlString, Me.BooNum)
    End Function

    Function BodyFormat_OLD(ByVal xml As String) As String

        Dim doc As New XmlDocument
        doc.LoadXml(xml)



        Dim sb As New Text.StringBuilder
        Dim hypen As String = "----------------------------------------------------------------------------------------"
        Dim message As String = "AUTOMATED GENERATED MESSAGE " & Body

        Try

            ''sb.Append(hypen & vbCrLf)
            sb.Append("CUSTOMER INFORMATION" & vbCrLf)
            sb.Append(hypen & vbCrLf)
            sb.AppendFormat("Title     :{0}{1}", doc.SelectSingleNode("Data/CustomerData/Title").InnerText, vbCrLf)
            sb.AppendFormat("First Name:{0}{1}", doc.SelectSingleNode("Data/CustomerData/FirstName").InnerText, vbCrLf)
            sb.AppendFormat("Last Name :{0}{1}", doc.SelectSingleNode("Data/CustomerData/LastName").InnerText, vbCrLf)
            sb.AppendFormat("Email     :{0}{1}", doc.SelectSingleNode("Data/CustomerData/Email").InnerText, vbCrLf)
            sb.AppendFormat("Telephone :{0}{1}", doc.SelectSingleNode("Data/CustomerData/PhoneNumber").InnerText, vbCrLf)
            sb.AppendFormat("Country   :{0}{1}", doc.SelectSingleNode("Data/RentalDetails/CountryCode").InnerText, vbCrLf)
            sb.AppendFormat("AgentCode :{0}{1}", doc.SelectSingleNode("Data/RentalDetails/AgentReference").InnerText, vbCrLf)

            sb.Append(hypen & vbCrLf)


            sb.Append(vbCrLf)
            sb.Append(vbCrLf)


            ''sb.Append(hypen & vbCrLf)
            sb.Append("PICKUP INFORMATION" & vbCrLf)
            sb.Append(hypen & vbCrLf)
            sb.AppendFormat("Pickup Date        :{0}{1}", doc.SelectSingleNode("Data/RentalDetails/CheckOutDateTime").InnerText, vbCrLf)
            sb.AppendFormat("Pickup Location    :{0}{1}", doc.SelectSingleNode("Data/RentalDetails/CheckOutLocationCode").InnerText, vbCrLf)
            sb.AppendFormat("DropOff Date       :{0}{1}", doc.SelectSingleNode("Data/RentalDetails/CheckInDateTime").InnerText, vbCrLf)
            sb.AppendFormat("DropOff Location   :{0}{1}", doc.SelectSingleNode("Data/RentalDetails/CheckInLocationCode").InnerText, vbCrLf)
            sb.Append(hypen & vbCrLf)


            sb.Append(vbCrLf)
            sb.Append(vbCrLf)

            If (doc.SelectSingleNode("Data/RentalDetails/ProductList").HasChildNodes) Then
                ''sb.Append(hypen & vbCrLf)
                sb.Append("PRODUCT LIST" & vbCrLf)
                sb.Append(hypen & vbCrLf)
                sb.AppendFormat("Quantity       Product Name" & vbCrLf)

                Dim nodes As XmlNodeList = doc.SelectNodes("Data/RentalDetails/ProductList/Product")
                Dim counter As Integer = 1
                For Each node As XmlNode In nodes
                    If (Not node.Attributes("PrdShortName") Is Nothing AndAlso Not node.Attributes("Qty") Is Nothing) Then
                        sb.AppendFormat("{0}) {1}           {2}{3}", counter, node.Attributes("Qty").Value, node.Attributes("PrdShortName").Value, vbCrLf)
                        counter = counter + 1
                    End If
                Next

                sb.Append(vbCrLf)
                sb.Append(vbCrLf)

            End If

            Dim temp As String = ReturnVehicleNameInformation(doc.SelectSingleNode("Data/RentalDetails/AvpId").InnerText, doc.SelectSingleNode("Data/RentalDetails/CheckOutDateTime").InnerText.Split(" ")(0).Trim)
            If Not String.IsNullOrEmpty(temp) Then

                Try
                    doc.LoadXml(temp)

                    sb.Append("VEHICLE INFORMATION" & vbCrLf)
                    sb.Append(hypen & vbCrLf)
                    sb.AppendFormat("Vehicle Name       :{0}{1}", doc.SelectSingleNode("VehInfo/sPrdName").InnerText, vbCrLf)
                    sb.AppendFormat("Vehicle Code       :{0}{1}", doc.SelectSingleNode("VehInfo/sPrdShortName").InnerText, vbCrLf)
                    sb.AppendFormat("Brand Name         :{0}{1}", doc.SelectSingleNode("VehInfo/sPkgBrdName").InnerText, vbCrLf)
                    sb.AppendFormat("T and C's          :{0}{1}", doc.SelectSingleNode("VehInfo/sPkgTCURL").InnerText, vbCrLf)
                    sb.AppendFormat("Package            :{0}{1}", doc.SelectSingleNode("VehInfo/sPkgDesc").InnerText, vbCrLf)
                    sb.Append(hypen & vbCrLf)


                    sb.Append(vbCrLf)
                    sb.Append(vbCrLf)

                Catch ex As Exception

                End Try

            End If

            sb.Append(hypen & vbCrLf)
            sb.Append(message & vbCrLf)
            sb.Append(hypen & vbCrLf)

        Catch ex As Exception
            LoggedEntry("BodyFormat: " & ex.Message)
        End Try

        Return sb.ToString
    End Function

    Function BodyFormat(ByVal XmlRequestString As String, ByVal xmlWSCallResult As XmlNode) As String

        XmlRequestString = XmlRequestString.Replace("</Data>", "<DebugInfo>" & XmlEncode(xmlWSCallResult.OuterXml) & "</DebugInfo></Data>")

        Dim xmlInput As New XmlDocument
        xmlInput.LoadXml(XmlRequestString)

        Dim transform As New XslTransform()
        transform.Load(System.AppDomain.CurrentDomain.BaseDirectory & "B2CFailureEmailTemplate.xsl")

        Using writer As New StringWriter
            transform.Transform(xmlInput, Nothing, writer, Nothing)
            Return writer.ToString()
        End Using

    End Function

    Function GetInnerText(ByVal Node As XmlNode) As String
        If Node Is Nothing Then
            Return String.Empty
        Else
            Return GetProperStringValue(Node.InnerText)
        End If
    End Function

    Private Function GetProperStringValue(ByVal InputString As String) As String
        If Not String.IsNullOrEmpty(InputString) AndAlso Not Trim(InputString).Equals(String.Empty) Then
            Return Trim(InputString)
        Else
            Return String.Empty
        End If
    End Function

    Public Function XmlEncode(ByVal strText As String) As String
        Dim aryChars As Integer() = {38, 60, 62, 34, 61, 39, 37, 58, 63}
        Dim i As Integer
        For i = 0 To UBound(aryChars)
            strText = Replace(strText, Chr(aryChars(i)), "&#" & aryChars(i) & ";")
        Next
        XmlEncode = strText
    End Function

#End Region

#Region "Constructor"
    Sub New()
        Try
            ReadConfigFile()
            CreateEventLogging()
        Catch ex As Exception
            LoggedEntry(ex.Message & "," & ex.StackTrace, EventLogEntryType.Error)
        End Try
    End Sub
#End Region

#Region "New Body format"
    Private Function ReturnVehicleNameInformation(ByVal sAvpId As String, ByVal pickupdate As String) As String

        Dim cmd As SqlCommand = Nothing
        Dim tempCon As SqlConnection
        Dim sb As New Text.StringBuilder
        sb.Append("")
        Try
            tempCon = Con()
            If tempCon.State = ConnectionState.Closed Then tempCon.Open()

            cmd = New SqlCommand("WEBP_getDetailsForAVPgetVehiclePrice", tempCon)

            cmd.Parameters.Add(New SqlParameter("sAvpId", SqlDbType.NVarChar, 64)).Value = sAvpId
            cmd.Parameters.Add(New SqlParameter("dCkoDate", SqlDbType.DateTime)).Value = "12/12/2009" ''pickupdate
            cmd.Parameters.Add(New SqlParameter("bIsLumpSum", SqlDbType.Bit)).Direction = ParameterDirection.Output
            cmd.Parameters.Add(New SqlParameter("bIsAgentCharge", SqlDbType.Bit)).Direction = ParameterDirection.Output
            cmd.Parameters.Add(New SqlParameter("spkgCode", SqlDbType.VarChar, 12)).Direction = ParameterDirection.Output
            cmd.Parameters.Add(New SqlParameter("sPkgCurrCode", SqlDbType.VarChar, 3)).Direction = ParameterDirection.Output
            cmd.Parameters.Add(New SqlParameter("sPkgDesc", SqlDbType.VarChar, 128)).Direction = ParameterDirection.Output
            cmd.Parameters.Add(New SqlParameter("sPkgId", SqlDbType.VarChar, 64)).Direction = ParameterDirection.Output
            cmd.Parameters.Add(New SqlParameter("sPkgComment", SqlDbType.VarChar, 256)).Direction = ParameterDirection.Output
            cmd.Parameters.Add(New SqlParameter("sPkgTCURL", SqlDbType.VarChar, 256)).Direction = ParameterDirection.Output
            cmd.Parameters.Add(New SqlParameter("sPkgInfoURL", SqlDbType.VarChar, 256)).Direction = ParameterDirection.Output
            cmd.Parameters.Add(New SqlParameter("sPkgBrdCode", SqlDbType.VarChar, 2)).Direction = ParameterDirection.Output
            cmd.Parameters.Add(New SqlParameter("sPkgBrdName", SqlDbType.VarChar, 12)).Direction = ParameterDirection.Output
            cmd.Parameters.Add(New SqlParameter("sClass", SqlDbType.VarChar, 12)).Direction = ParameterDirection.Output
            cmd.Parameters.Add(New SqlParameter("sType", SqlDbType.VarChar, 12)).Direction = ParameterDirection.Output
            cmd.Parameters.Add(New SqlParameter("sPrdShortName", SqlDbType.VarChar, 64)).Direction = ParameterDirection.Output
            cmd.Parameters.Add(New SqlParameter("sPrdName", SqlDbType.VarChar, 128)).Direction = ParameterDirection.Output
            cmd.Parameters.Add(New SqlParameter("sPrdInfoURL", SqlDbType.VarChar, 128)).Direction = ParameterDirection.Output
            cmd.Parameters.Add(New SqlParameter("sAprList", SqlDbType.NVarChar, 4000)).Direction = ParameterDirection.Output

            cmd.CommandType = CommandType.StoredProcedure
            cmd.ExecuteNonQuery()


            sb.Append("<VehInfo>")
            For Each p As SqlParameter In cmd.Parameters
                If p.Direction = ParameterDirection.Output Then
                    sb.AppendFormat("<{0}>{1}</{2}>", p.ParameterName, p.Value, p.ParameterName)
                End If
            Next
            sb.Append("</VehInfo>")
        Catch ex As Exception
            LoggedEntry("ReturnVehicleNameInformation: " & BooNum & ":" & ex.Message & "," & ex.StackTrace, EventLogEntryType.Error)
        End Try

        tempCon = Nothing
        cmd = Nothing
        Return sb.ToString
    End Function


#End Region

End Class
