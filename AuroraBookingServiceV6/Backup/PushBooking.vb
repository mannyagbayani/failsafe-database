Imports System.ServiceProcess
Public Class PushBooking



    Protected Overrides Sub OnStart(ByVal args() As String)
        ' Add code here to start your service. This method should set things
        ' in motion so your service can do its work.
        Dim util As New Utility
        Timer1.Interval = (60000 * util.minutesToRefresh)
        Timer1.Enabled = True
        util.LoggedEntry(String.Concat("APPCONFIG SETTINGS: ", _
                                       " NoOfAttempts: ", util.NoOfAttempts, _
                                       " maintainLogged: ", util.maintainLogged, _
                                       " minutesToRefresh: ", util.minutesToRefresh, _
                                       " DB: ", util.ConnectionString, _
                                       " WS: ", util.WSUrl))

        util.IsFirstLoad = True
    End Sub

    Protected Overrides Sub OnStop()
        ' Add code here to perform any tear-down necessary to stop your service.
        Timer1.Enabled = False
    End Sub

    Private Sub Timer1_Elapsed(ByVal sender As System.Object, ByVal e As System.Timers.ElapsedEventArgs) Handles Timer1.Elapsed
        Dim util As New Utility
        Dim retvalue As String = util.PushToAurora()
        util.LoggedEntry("timer :" & DateTime.Now.ToLongTimeString)
        ''Timer1.Start()

    End Sub

    Public WriteOnly Property start() As Boolean
        Set(ByVal value As Boolean)
            Timer1.Start()
        End Set
    End Property



End Class
